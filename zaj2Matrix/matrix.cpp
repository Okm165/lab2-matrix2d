#include <iostream>
#include <string>
#include <stdexcept> // std::out_of_range()
#include <iomanip>   // std::setw()

using namespace std;

#include "matrix.h"

std::ostream &operator<<(std::ostream &os, const TwoDimensionMatrix &matrix)
{
    // Wypisz macierz w formacie "{ a, b }{ c, d }"
    os << "{ " << matrix.matrix_[0][0] << " " << matrix.matrix_[0][1] << " }\n";
    os << "{ " << matrix.matrix_[1][0] << " " << matrix.matrix_[1][1] << " }";
    return os;
}

std::istream &operator>>(std::istream &is, TwoDimensionMatrix &matrix)
{
    // Wczytaj macierz w formacie "{ a, b }{ c, d }"
    char c;
    is >> c >> matrix.matrix_[0][0] >> c >> matrix.matrix_[0][1] >> c >> c;
    is >> c >> matrix.matrix_[1][0] >> c >> matrix.matrix_[1][1] >> c >> c;
    return is;
}

// #ifndef _MSC_FULL_VER // if not Visual Studio Compiler
//     #warning "Klasa jest do zaimplementowania. Instrukcja w pliku naglowkowym"
// #else
//     #pragma message ("Klasa jest do zaimplementowania. Instrukcja w pliku naglowkowym")
// #endif

